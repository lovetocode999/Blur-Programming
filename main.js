function toDataUrl(url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.onload = function() {
		var reader = new FileReader();
		reader.onloadend = function() {
			callback(reader.result);
		}
		reader.readAsDataURL(xhr.response);
	};
	xhr.open('GET', url);
	xhr.responseType = 'blob';
	xhr.send();
}
function run() {
	if (document.getElementById("Stage-Script")) {
		document.getElementById("Stage-Script").parentNode.removeChild(document.getElementById("Stage-Script"));
	}
	var myNode = document.getElementById("Stage");
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}

	stagescript = document.createElement("script");
	stagescript.id = "Stage-Script";
	stagescript.text = document.getElementById('startStageScript').value+document.getElementById("mainStageScript").value;
	document.body.appendChild(stagescript);
}
function newSprite() {
	var user = prompt("Sorry, this feature is not working yet.", "Okay");
	if (user == "lovetocode999") {
		var spriteName = prompt("Sprite Name:", "sprite1");
		var spriteCostume = prompt("Sprite Default Costume", "https://example.costume.url");
		var emptyContent = "</tr><tr>";
		document.getElementById("emptyRow").innerHTML = emptyContent;
	}
}
function changeSpriteName(sprite, newName){
	if (spriteNameList.indexOf(newName) == -1 && newName && newName !== "stage") {
		var modScript = document.getElementById("startStageScript").value;
		modScript = modScript.replace("var "+spriteNameList[sprite]+" =", "var "+newName+" =");
		modScript = modScript.replace(spriteNameList[sprite]+".addTo", newName+".addTo");
		document.getElementById("startStageScript").value = modScript;
		spriteNameList[sprite] = newName;
		document.getElementsByClassName("sprite-label")[sprite].text = newName;
		if (document.getElementById("Stage-Script")) {
			document.getElementById("Stage-Script").parentNode.removeChild(document.getElementById("Stage-Script"));
		}
		var myNode = document.getElementById("Stage");
		while (myNode.firstChild) {
			myNode.removeChild(myNode.firstChild);
		}
		stagescript = document.createElement("script");
		stagescript.id = "Stage-Script";
		stagescript.text = document.getElementById('startStageScript').value;
		document.body.appendChild(stagescript);
	} else if (newName == "stage") {
		alert('Error: You can\'t name a sprite "stage"');
	} else if (newName) {
		alert("Error: "+newName+" is already a sprite");
	} else{
		alert("Error: Failure renaming sprite");
	}
}
function changeSpriteImage(sprite, newImage) {
	if (spriteImageList.indexOf(newImage) == -1 && newImage && testImage(newImage)) {
		var modScript = document.getElementById("startStageScript").value;
		modScript = modScript.replace("image: '"+spriteImageList[sprite]+"'", "image: '"+newImage+"'");
		document.getElementById("startStageScript").value = modScript;
		spriteImageList[sprite] = newImage;
		document.getElementsByClassName("sprite-image")[sprite].src = newImage;
		
		if (document.getElementById("Stage-Script")) {
			document.getElementById("Stage-Script").parentNode.removeChild(document.getElementById("Stage-Script"));
		}
		var myNode = document.getElementById("Stage");
		while (myNode.firstChild) {
			myNode.removeChild(myNode.firstChild);
		}
		stagescript = document.createElement("script");
		stagescript.id = "Stage-Script";
		stagescript.text = document.getElementById('startStageScript').value;
		document.body.appendChild(stagescript);
	} else {
		alert("Error: Failure changing sprite image");
	}
}
run.onerror = function errorHandler(errorMsg, url, lineNumber) {
	alert("code execution returned the error \n"+errorMsg+"\n try checking your sprite names for illegal characters \n If your sprites aren't causing the error, it must be your code");
}
textareas[1].onkeydown = function(e){
    if(e.keyCode==9 || e.which==9){
        e.preventDefault();
        var s = this.selectionStart;
        this.value = this.value.substring(0,this.selectionStart) + "\t" + this.value.substring(this.selectionEnd);
        this.selectionEnd = s+1; 
    }
}
function testImage(url) {
	return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);
}